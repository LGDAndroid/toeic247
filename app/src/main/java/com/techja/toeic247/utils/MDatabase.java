package com.techja.toeic247.utils;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.techja.toeic247.model.Audio;

import java.util.ArrayList;
import java.util.List;

//Đối tượng kiểu singleton, quản lý việc upload, download database
public class MDatabase {
    private static MDatabase instance;
    private DatabaseReference mData;
    private List<Audio> listAudio;

    private MDatabase() {
        //for singleton
    }

    public static MDatabase getInstance() {
        if(instance == null){
            instance = new MDatabase();
        }
        return instance;
    }

    public void themAudio(Audio audio) {
        mData = FirebaseDatabase.getInstance().getReference();
        mData.child("Audio").push().setValue(audio, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if(databaseError == null){
                    Log.e("TAG","Thành công!");
                }else{
                    Log.e("TAG","Thất bại!");
                }
            }
        });
    }

    public void loadListAudio(){
        listAudio = new ArrayList<>();
        mData = FirebaseDatabase.getInstance().getReference();
        mData.child("Audio").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Audio audio = dataSnapshot.getValue(Audio.class);
                if(audio != null){
                    listAudio.add(audio);
                }else{

                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public List<Audio> getListAudio(){
        return listAudio;
    }
}
