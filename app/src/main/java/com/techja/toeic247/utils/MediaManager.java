package com.techja.toeic247.utils;

import android.database.Cursor;
import android.media.MediaPlayer;

import com.techja.toeic247.model.Audio;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MediaManager {
    /*3 trạng thái của 1 bài hát*/

    public static final int STATE_IDLE = 1;
    public static final int STATE_PLAYING = 2;
    public static final int STATE_PAUSED = 3;

    private MediaPlayer mPlayer;

    //TAG
    private static final String TAG = MediaManager.class.getName();
    private static MediaManager instance;
    private List<Audio> mListAudio;

    private int currentIndex;
    private int mState = STATE_IDLE;

    private MediaManager() {
        //for singleton
        mPlayer = new MediaPlayer();
        currentIndex = 0;
    }

    public static MediaManager getInstance() {
        if (instance == null) {
            instance = new MediaManager();
        }
        return instance;
    }

    public List<Audio> getListSong() {
        return mListAudio;
    }

    public void play() {
        try {
            //Nếu state của bài hát đang ở trạng thái idle
            if (mState == STATE_IDLE) {
                //Reset lại mPlayer để set lại url
                mPlayer.reset();
                //Lấy Url của bài hát
                String songPath = mListAudio.get(currentIndex).getUrl();
                //set dataSource cho mPlayer
                mPlayer.setDataSource(songPath);
                //chuyển sang trạng thái prepared để sẵn sàng phát nhạc
                mPlayer.prepare();
                //Start bài hát
                mPlayer.start();
                //Chuyển trạng thái của bài hát sang playing
                mState = STATE_PLAYING;
            } //Nếu trạng thái của bài hát là playing
            else if (mState == STATE_PLAYING) {
                //Tạm dừng bài hát
                mPlayer.pause();
                //Set trạng thái hiện tại của bài hát là paused
                mState = STATE_PAUSED;
            } else if (mState == STATE_PAUSED) {
                mPlayer.start();
                mState = STATE_PLAYING;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void next() {
        resetState();
        currentIndex++;
        if (currentIndex >= mListAudio.size()) {
            currentIndex = 0;
        }
        play();
    }

    public void back() {
        resetState();
        currentIndex--;
        if (currentIndex < 0) {
            currentIndex = mListAudio.size() - 1;
        }
        play();
    }

    public Audio getCurrentSong() {
        return mListAudio.get(currentIndex);
    }

    public void setCurrentSong(Audio song) {
        currentIndex = mListAudio.indexOf(song);
    }

    public int getState() {
        return mState;
    }

    public void resetState() {
        mState = STATE_IDLE;
    }

    public String getCurrentTime() {
        int time = mPlayer.getCurrentPosition();
        if (time < 0) return "00:00";

        SimpleDateFormat df = new SimpleDateFormat("mm:ss");
        return df.format(time);
    }

    public int getCurrentPos() {
        return mPlayer.getCurrentPosition();
    }

    public int getTotalPos() {
        return mPlayer.getDuration();
    }

    public int getTimeSecondAudio()
    {return mPlayer.getDuration();
    }

    public String getTotalTime() {
        int time = mPlayer.getDuration();
        if (time < 0) return "00:00";

        SimpleDateFormat df = new SimpleDateFormat("mm:ss");
        return df.format(time);
    }

    public void seekTo(int progress) {
        mPlayer.seekTo(progress);
    }

    public void stop() {
        mPlayer.reset();
    }

    public void loadListAudio(){
        mListAudio = new ArrayList<>();
        mListAudio = MDatabase.getInstance().getListAudio();
    }

    public int getCurrentIndex(){
        return currentIndex;
    }
}
