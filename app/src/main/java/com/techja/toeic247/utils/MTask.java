package com.techja.toeic247.utils;

import android.os.AsyncTask;
import android.util.Log;

public final class MTask extends AsyncTask<Object, Object, Object> {
    private static final String TAG = MTask.class.getName();
    private OnAsyncCallBack callBack;
    private int key;

    public MTask(OnAsyncCallBack callBack, int key) {
        this.key = key;
        this.callBack = callBack;
    }

    @Override
    protected void onPreExecute() {
        callBack.initTask();
    }

    @Override
    protected Object doInBackground(Object... data) {
        Log.i(TAG, "doInBackground..." + key);
        return callBack.execTask(this, key, data);
    }

    public void updateTask(Object... data) {
        publishProgress(data);
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        callBack.updateUI(key, values);
    }

    @Override
    protected void onPostExecute(Object result) {
        callBack.taskComplete(key, result);
    }

    public void start(Object... data) {
        execute(data);
    }

    public void startAsync(Object... data) {
        executeOnExecutor(THREAD_POOL_EXECUTOR, data);
    }

    public interface OnAsyncCallBack {
        default void initTask() {
        }

        Object execTask(MTask task, int key, Object... data);

        default void updateUI(int key, Object... values) {
        }

        default void taskComplete(int key, Object result) {
        }
    }
}
