package com.techja.toeic247.view.fragment;

import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.techja.toeic247.App;
import com.techja.toeic247.R;
import com.techja.toeic247.callback.OnPart2CallBackToView;
import com.techja.toeic247.model.Audio;
import com.techja.toeic247.presenter.Part2Presenter;
import com.techja.toeic247.utils.MTask;
import com.techja.toeic247.utils.MediaManager;
import com.techja.toeic247.view.event.OnActionCallBack;

import java.util.List;

public class FrgPart2 extends BaseFragment<Part2Presenter, OnActionCallBack> implements OnPart2CallBackToView, SeekBar.OnSeekBarChangeListener, MTask.OnAsyncCallBack {
    public static final String TAG = FrgPart2.class.getName();
    private static final int KEY_START_PLAYER = 101;
    private ImageView ivBack, ivFavorite, ivList, ivPlay;
    private TextView tvNumber, tvQuestion, tvCurrentTime, tvDuration;
    private RadioButton rdAnswer1, rdAnswer2, rdAnswer3;
    private SeekBar seekBar;
    private Button btAnswer;
    private List<Audio> audioList;
    private MTask mTask;
    private boolean isStop;
    private boolean isStart;

    @Override
    protected void initViews() {
        mTask = new MTask(this, KEY_START_PLAYER);
        mTask.startAsync();

        ivBack = findViewById(R.id.iv_back,this);
        ivFavorite = findViewById(R.id.iv_favorite,this);
        ivList = findViewById(R.id.iv_list,this);
        ivPlay = findViewById(R.id.iv_play,this);

        tvNumber = findViewById(R.id.tv_number);
        tvQuestion = findViewById(R.id.tv_question);
        tvCurrentTime = findViewById(R.id.tv_current_time);
        tvDuration = findViewById(R.id.tv_duration);

        rdAnswer1 = findViewById(R.id.radio_a,this);
        rdAnswer2 = findViewById(R.id.radio_b,this);
        rdAnswer3 = findViewById(R.id.radio_c,this);

        seekBar = findViewById(R.id.seek_bar);
        seekBar.setProgress(0);
        seekBar.setOnSeekBarChangeListener(this);

        btAnswer = findViewById(R.id.bt_answer,this);

        // load list audio
        mPresenter.loadAudio();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_part_2;
    }

    @Override
    protected Part2Presenter getPresenter() {
        return new Part2Presenter(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (isStart) MediaManager.getInstance().seekTo(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isStart = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isStart = false;
    }

    @Override
    public void setTvNumber(int current, int total) {
        tvNumber.setText(current + "/" + total);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.iv_play){
            playAudio();
        }
        super.onClick(v);
    }

    private void playAudio() {
        MediaManager.getInstance().play();
        tvDuration.setText(MediaManager.getInstance().getTotalTime());
        seekBar.setMax(MediaManager.getInstance().getTimeSecondAudio());
    }

    @Override
    public Object execTask(MTask task, int key, Object... data) {
        isStop = false;
        while (!isStop) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (MediaManager.getInstance().getState() != MediaManager.STATE_IDLE) {
                task.updateTask();
            }
        }
        return null;
    }

    @Override
    public void onDestroy() {
        isStop = true;
        mTask.cancel(true);
        MediaManager.getInstance().stop();
        super.onDestroy();
    }

    @Override
    public void updateUI(int key, Object... values) {
        tvCurrentTime.setText(MediaManager.getInstance().getCurrentTime());
        seekBar.setProgress(MediaManager.getInstance().getCurrentPos());
    }

    @Override
    public void taskComplete(int key, Object result) {

    }
}
