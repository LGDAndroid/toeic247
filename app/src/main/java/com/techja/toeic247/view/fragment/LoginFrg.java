package com.techja.toeic247.view.fragment;

import android.media.MediaPlayer;
import android.util.Log;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.techja.toeic247.App;
import com.techja.toeic247.R;
import com.techja.toeic247.callback.OnLoginCallBackToView;
import com.techja.toeic247.model.Audio;
import com.techja.toeic247.presenter.LoginPresenter;
import com.techja.toeic247.utils.MDatabase;
import com.techja.toeic247.view.adapter.LoginAdapter;
import com.techja.toeic247.view.event.OnActionCallBack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class LoginFrg extends BaseFragment<LoginPresenter, OnActionCallBack> implements OnLoginCallBackToView {
    public static final String TAG = LoginFrg.class.getName();
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void initViews() {
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);

        tabLayout.addTab(tabLayout.newTab().setText("Đăng Nhập"));
        tabLayout.addTab(tabLayout.newTab().setText("Đăng Ký"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final LoginAdapter adapter = new LoginAdapter(getFragmentManager(), mContext, tabLayout.getTabCount());
        adapter.setCallBack(callback);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

//        MediaPlayer mediaPlayer = new MediaPlayer();
//        try {
//            mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F11.mp3?alt=media&token=381bdc35-79fa-4add-9100-421efc50f845");
//            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mp) {
//                    mp.start();
//                }
//            });
//            mediaPlayer.prepare();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    private void addDataToFireBase() {
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F11.mp3?alt=media&token=381bdc35-79fa-4add-9100-421efc50f845"
                , "What time do you usually have lunch?",
                        "I’m not very hungry.",
                        "We usually go to the cafeteria.",
                        "We eat at 12 o’clock.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F12.mp3?alt=media&token=25ab0026-600e-45e0-a885-ffbe4e049c54"
                        , "Did you enjoy the company picnic?",
                        "Saturday, I think.",
                        "Yes, too bad it was canceled.",
                        "I thought it was boring.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F13.mp3?alt=media&token=13a163ec-402e-4583-bc65-5f2379ff8b22"
                        , "Where are the paper cups and napkins?",
                        "On the third shelf from the top.",
                        "Five of each, please.",
                        "Help yourself.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F14.mp3?alt=media&token=dafb1f4b-e8ca-4183-8f0a-3f66370d2840"
                        , "How about taking a trip to New York this year?",
                        "Sorry, I’m too busy.",
                        "Our company may relocate.",
                        "Take the highway No. 400.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F15.mp3?alt=media&token=70dbb252-dc19-4d40-94c9-b0b452412f28"
                        , "Why did you quit your excellent job?",
                        "I liked all of my colleagues.",
                        "There were several of them.",
                        "It was time for a change in my life.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F16.mp3?alt=media&token=9ae6c134-95af-40dd-b5e5-cfafa98a6622"
                        , "Who was the man at the bar last night?",
                        "Just a drink or two.",
                        "That was a client from England.",
                        "It’s my favorite bar.", 2));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F17.mp3?alt=media&token=f493aa40-25bc-4ced-bb75-9d740e4255cd"
                        , "How far is it to the convention center from here?",
                        "I hate traffic jams.",
                        "Yes, right in the center of town.",
                        "A taxi will get you there in 20 minutes.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F18.mp3?alt=media&token=d9132431-83cd-4a54-b112-1e566b2119d5"
                        , "Do you know anyone who wants to buy a computer?",
                        "Yes, they’re getting cheaper all the time.",
                        "Only the software industry keeps growing.",
                        "No, but you might try placing a classified ad.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F19.mp3?alt=media&token=2b6fd94d-cbb9-440b-bb4b-04f2db9c6fda"
                        , "Didn’t you know we had a meeting with clients today?",
                        "Yes, he’s one of my best clients.",
                        "I thought it was tomorrow.",
                        "I’ll be ready by Thursday.", 2));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F20.mp3?alt=media&token=c03f6bd7-daeb-439f-8461-d998f9ff6af6"
                        , "We should be watching the exchange rates more closely, shouldn’t we?",
                        "We will be in trouble if they do.",
                        "Yes, but only in the past.",
                        "No, they’re not that important to us.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F21.mp3?alt=media&token=76133756-5a34-4c1d-9c71-15f27371fb37"
                        , "Are you sure you don’t need any help?",
                        "No, I’m quite sure of that.",
                        "Well, that’s reassuring.",
                        "Yes, thanks. I’ll be fine.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F22.mp3?alt=media&token=7e28cc06-6424-40a9-afd9-9380f076ea30"
                        , "I didn’t like the movie so much. How about you?",
                        "It was captured on film.",
                        "I thought it was very exciting.",
                        "Seeing movies is a lot of fun.", 2));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F23.mp3?alt=media&token=630c24b1-fa95-4d99-bb28-64645daf94ff"
                        , "Where do you keep your inventory?",
                        "We store it in a warehouse on the waterfront.",
                        "It’s the last one we have in stock.",
                        "Hurry! Supplies are running out.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F24.mp3?alt=media&token=ba819957-9dfd-4c26-b701-64d35984d40a"
                        , "Has the shipment of engine parts arrived yet?",
                        "It won’t be here until next week.",
                        "The shipment was later than that.",
                        "It’ll be shipped by plane.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F25.mp3?alt=media&token=469f9051-4973-4e5d-9f32-465ade101335"
                        , "Would you like that suit made in navy blue or dark brown?",
                        "I don’t think it really suits me.",
                        "I think I’ll go with the brown.",
                        "I got out of the Navy two years ago.", 2));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F26.mp3?alt=media&token=0412d64e-c954-4bb5-a8af-d355792bde30"
                        , "When will we know whether we made a profit this year?",
                        "We’ll have those figures ready for you tomorrow morning.",
                        "They filed for bankruptcy last month.",
                        "We were asked to fill the whole order by tonight.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F27.mp3?alt=media&token=4ceba8d6-c03f-4716-aa63-72e8604dbf50"
                        , "How did you find such a great travel agency?",
                        "The owner is one of my old friends.",
                        "No, the price is very expensive.",
                        "Sure, I can introduce you tomorrow.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F28.mp3?alt=media&token=71f356ee-8650-4b3c-b27e-8bc80b493d4d"
                        , "Do I need to know Spanish to get this position?",
                        "Language study benefits children the most.",
                        "Spanish is one of romantic languages.",
                        "If you’re planning to keep the job permanently.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F29.mp3?alt=media&token=65a505fa-4307-4099-b411-d62e3bfea891"
                        , "Should I throw this old typewriter away?",
                        "He said he’d write it for you.",
                        "Keep it. It’s practically an antique.",
                        "Typing can be a hassle.", 2));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F30.mp3?alt=media&token=dd9c00da-8161-4399-b497-0181b350313b"
                        , "What are you going to tell your boss about the Lanesome account?",
                        "I’ll be honest and let him know that the client opened a new one.",
                        "My boss is a very talented person.",
                        "She never told me a thing about it.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F31.mp3?alt=media&token=4c27cd8c-e910-4ce5-8ad6-4ddbad5932c6"
                        , "Why wasn’t Maria here in the office yesterday?",
                        "I heard she was sent on a business trip.",
                        "Because the office was too small.",
                        "It’s on the third floor.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F32.mp3?alt=media&token=92f2ff95-bb0d-4705-94c6-7625207dcded"
                        , "You are finished with this printer, aren’t you?",
                        "I think the colors are poorly chosen.",
                        "The printer was out-of-date.",
                        "Just about. Give me another ten minutes.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F33.mp3?alt=media&token=3f7066a9-0125-486b-b410-3feb2d649733"
                        , "Would you mind turning on the air conditioner?",
                        "Do you mean it’s hot in here.",
                        "He reminded me yesterday.",
                        "Yes, we need fresh air.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F34.mp3?alt=media&token=25691e2a-c52f-450d-8544-a46325b41f46"
                        , "What do you think about the new no-smoking rule?",
                        "Yes, I think I will.",
                        "It starts from next month.",
                        "It’s OK with me. I don’t smoke.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F35.mp3?alt=media&token=b264d01d-ffde-431f-8cb2-31f7f2b3c606"
                        , "Why are you using my computer?",
                        "Computer work is very boring.",
                        "Sorry about that. Mine is broken.",
                        "Yes, I’m depend on you.", 2));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F36.mp3?alt=media&token=b9b9b21c-f3c5-4fb0-a3c7-3fd9fead4250"
                        , "Don’t you ever get tired of commuting?",
                        "Sure, but what choice do I have.",
                        "Those people look really tired.",
                        "At seven o’clock every morning.", 1));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F37.mp3?alt=media&token=c6e91773-0cc7-419b-a099-ad0ee2b66afe"
                        , "When does the next downtown train arrive?",
                        "That wasn’t very long.",
                        "Yes, it’s an express train.",
                        "Not for another 15 minutes.", 3));
//
        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F38.mp3?alt=media&token=8e73da2a-567f-4d0c-8112-e6e0a154db7d"
                        , "Can you send these documents by overnight express to Paris?",
                        "We should reserve the train tickets first.",
                        "I’ll do it right after finishing this.",
                        "I prefer flying rather than driving.", 2));

        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F39.mp3?alt=media&token=985ecc20-7603-4edb-9050-88c2b3732dfb"
                        , "I think I hear someone’s cellphone ringing.",
                        "She was trying to call us about the new contract.",
                        "I’ll call you back when I’m done with this.",
                        "It can’t be mine. I turned mine off.", 3));

        MDatabase.getInstance().
                themAudio(new Audio("https://firebasestorage.googleapis.com/v0/b/toeic-247.appspot.com/o/%C4%90%E1%BB%81%2011%2F40.mp3?alt=media&token=69d7f6b9-c03c-4390-a82e-2afbfeba41ac"
                        , "Why didn’t you buy the stock when it was cheap?",
                        "I didn’t know the price was going to go up.",
                        "You’re right. It’s stuck right here.",
                        "Yes, it’s very reasonable.", 1));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected LoginPresenter getPresenter() {
        return new LoginPresenter(this);
    }
}
