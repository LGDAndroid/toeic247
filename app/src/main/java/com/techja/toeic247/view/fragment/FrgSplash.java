package com.techja.toeic247.view.fragment;

import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.techja.toeic247.R;
import com.techja.toeic247.callback.OnSplashCallBackToView;
import com.techja.toeic247.presenter.SplashPresenter;
import com.techja.toeic247.utils.MDatabase;
import com.techja.toeic247.view.event.OnActionCallBack;

public class FrgSplash extends BaseFragment<SplashPresenter, OnActionCallBack> implements OnSplashCallBackToView {
    public static final String TAG = FrgSplash.class.getName();
    private ImageView ivLogo, ivName;
    private TextView tvAdvice;
    private Animation topAnimation, bottomAnimation;

    @Override
    protected void initViews() {

        ivLogo = findViewById(R.id.iv_logo);
        ivName = findViewById(R.id.iv_toiec247);
        tvAdvice = findViewById(R.id.tv_advice);
        tvAdvice.setText("Hãy dành 30 phút luyện tập Tiếng Anh mỗi ngày!");

        //Hooks
        topAnimation = AnimationUtils.loadAnimation(mContext,R.anim.top_animation);
        bottomAnimation = AnimationUtils.loadAnimation(mContext,R.anim.bottom_animation);
        ivLogo.setAnimation(topAnimation);
        ivName.setAnimation(bottomAnimation);
        tvAdvice.setAnimation(bottomAnimation);
        MDatabase.getInstance().loadListAudio();
        doTaskDelay();
    }

    private void doTaskDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doTask();
            }
        }, 2000);
    }

    private void doTask() {
        // Kiểm tra User xem đã đăng nhập chưa
        // Nếu đăng nhập rồi => continue
        // Nếu chưa đăng nhập => show Frg login/register
        callback.showFrgLogin();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected SplashPresenter getPresenter() {
        return new SplashPresenter(this);
    }
}
