package com.techja.toeic247.view.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.techja.toeic247.R;
import com.techja.toeic247.callback.OnSignUpCallBackToView;
import com.techja.toeic247.presenter.SignUpPresenter;
import com.techja.toeic247.view.event.OnActionCallBack;

public class SignUpFrg extends BaseFragment<SignUpPresenter, OnActionCallBack> implements OnSignUpCallBackToView {
    private EditText edtEmail, edtPassword, edtRePassWord;
    private Button btDangKy;
    private FirebaseAuth mAuth;

    @Override
    protected void initViews() {
        mAuth = FirebaseAuth.getInstance();
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        edtRePassWord = findViewById(R.id.edt_repassword);
        btDangKy = findViewById(R.id.bt_dang_ky,this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.register_tab_fragment;
    }

    @Override
    protected SignUpPresenter getPresenter() {
        return new SignUpPresenter(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.bt_dang_ky){
            mPresenter.dangKy(edtEmail.getText().toString(), edtPassword.getText().toString(), edtRePassWord.getText().toString(), mContext);
        }
    }

    @Override
    public void showNotify(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }
}
