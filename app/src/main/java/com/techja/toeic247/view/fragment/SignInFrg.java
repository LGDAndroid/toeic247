package com.techja.toeic247.view.fragment;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.internal.ShowFirstParty;
import com.techja.toeic247.R;
import com.techja.toeic247.callback.OnSignInCallBackToView;
import com.techja.toeic247.presenter.SignInPresenter;
import com.techja.toeic247.view.event.OnActionCallBack;

public class SignInFrg extends BaseFragment<SignInPresenter, OnActionCallBack> implements OnSignInCallBackToView {
    private EditText edtEmail, edtPassWord;
    private TextView tvForgetPass;
    private Button btLogin;
    private ImageView ivShowPass;

    private static final int LEVEL_HIDDEN = 0;
    private static final int LEVEL_SHOW = 1;
    private int count = 0;

    @Override
    protected void initViews() {
        edtEmail = findViewById(R.id.edt_email);
        edtPassWord = findViewById(R.id.edt_password);
        tvForgetPass = findViewById(R.id.tv_quen_pass);
        btLogin = findViewById(R.id.bt_dang_nhap,this);
        ivShowPass = findViewById(R.id.iv_show_pass,this);

        edtEmail.setTranslationX(800);
        edtPassWord.setTranslationX(800);
        tvForgetPass.setTranslationX(800);
        btLogin.setTranslationX(800);
        ivShowPass.setTranslationX(800);

        edtEmail.setAlpha(0);
        edtPassWord.setAlpha(0);
        tvForgetPass.setAlpha(0);
        btLogin.setAlpha(0);

        edtEmail.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        edtPassWord.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        tvForgetPass.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        btLogin.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(700).start();
        ivShowPass.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.login_tab_fragment;
    }

    @Override
    protected SignInPresenter getPresenter() {
        return new SignInPresenter(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.bt_dang_nhap){
            mPresenter.dangNhap(edtEmail.getText().toString(), edtPassWord.getText().toString(), mContext);
        }else if(v.getId() == R.id.iv_show_pass){
            checkShowPass();
        }
    }

    private void checkShowPass() {
        if(count == LEVEL_HIDDEN){
            count = LEVEL_SHOW;
            ivShowPass.setImageLevel(LEVEL_SHOW);

            edtPassWord.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }else{
            count = LEVEL_HIDDEN;
            ivShowPass.setImageLevel(LEVEL_HIDDEN);

            edtPassWord.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    @Override
    public void showNotify(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showFrgMenu() {
        callback.showFrgMenu();
    }
}
