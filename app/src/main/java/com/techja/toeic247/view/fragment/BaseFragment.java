package com.techja.toeic247.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.techja.toeic247.presenter.BasePresenter;
import com.techja.toeic247.view.event.OnActionCallBack;

public abstract class BaseFragment<K extends BasePresenter, C extends OnActionCallBack> extends Fragment implements View.OnClickListener {
    protected Context mContext;
    protected View rootView;
    protected K mPresenter;
    protected C callback;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mPresenter = getPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(getLayoutId(), container, false);
        initViews();
        return rootView;
    }

    public final void setOnActionCallBack(C event) {
        callback = event;
    }

    protected final <T extends View> T findViewById(int id, View.OnClickListener event) {
        T view = rootView.findViewById(id);
        if (view != null && event != null) {
            view.setOnClickListener(event);
        }
        return view;
    }

    protected final <T extends View> T findViewById(int id) {
        return findViewById(id, null);
    }

    @Override
    public void onClick(View v) {
        //do nothing here
    }

    @Override
    public void onDestroy() {
        rootView = null;
        mContext = null;
        super.onDestroy();
    }

    protected abstract void initViews();

    protected abstract int getLayoutId();

    protected abstract K getPresenter();
}
