package com.techja.toeic247.view.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.techja.toeic247.callback.OnCallBackToView;
import com.techja.toeic247.view.event.OnActionCallBack;
import com.techja.toeic247.view.fragment.SignInFrg;
import com.techja.toeic247.view.fragment.SignUpFrg;

public class LoginAdapter extends FragmentPagerAdapter{
    private Context mContext;
    private int totalTabs;
    private OnActionCallBack callback;

    public LoginAdapter(FragmentManager fm, Context mContext, int totalTabs) {
        super(fm);
        this.mContext = mContext;
        this.totalTabs = totalTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                SignInFrg signInFrg = new SignInFrg();
                signInFrg.setOnActionCallBack(callback);
                return signInFrg;
            case 1:
                SignUpFrg signUpFrg = new SignUpFrg();
                signUpFrg.setOnActionCallBack(callback);
                return signUpFrg;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }

    public void setCallBack(OnActionCallBack callback){
        this.callback = callback;
    }
}
