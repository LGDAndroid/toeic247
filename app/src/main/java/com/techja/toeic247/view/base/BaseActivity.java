package com.techja.toeic247.view.base;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.techja.toeic247.presenter.BasePresenter;

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements View.OnClickListener{
    protected T mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        mPresenter = getPresenter();
        initViews();
    }

    protected abstract T getPresenter();

    protected abstract void initViews();

    protected abstract int getLayoutId();

    //Generic Java
    public <T extends View> T findViewById(int id, View.OnClickListener event) {
        T v = findViewById(id);
        if (v != null && event != null) {
            v.setOnClickListener(event);
        }
        return v;
    }

    protected String textOf(TextView v) {
        if (v == null) return null;
        return v.getText().toString();
    }

    @Override
    public void onClick(View v) {
        //do nothing
    }
}
