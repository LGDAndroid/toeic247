package com.techja.toeic247.view.event;

public interface OnActionCallBack {
    default void callBack(String key, Object data){
        //for use late
    }

    void showFrgLogin();

    void showFrgMenu();
}
