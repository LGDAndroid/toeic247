package com.techja.toeic247;

import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.Nullable;

import com.techja.toeic247.callback.OnHomeCallBackToView;
import com.techja.toeic247.presenter.HomePresenter;
import com.techja.toeic247.view.base.BaseActivity;
import com.techja.toeic247.view.event.OnActionCallBack;
import com.techja.toeic247.view.fragment.BaseFragment;
import com.techja.toeic247.view.fragment.FrgPart2;
import com.techja.toeic247.view.fragment.FrgSplash;
import com.techja.toeic247.view.fragment.LoginFrg;

import java.lang.reflect.Constructor;

public class HomeActivity extends BaseActivity<HomePresenter> implements OnHomeCallBackToView, OnActionCallBack {

    //TAG: lưu getName của Frg đang được show
    private String TAG = "";

    @Override
    protected HomePresenter getPresenter() {
        return new HomePresenter(this);
    }

    @Override
    protected void initViews() {
        //Nếu TAG khác rỗng thì show Frg
        if(!TAG.isEmpty()){
            showFrg(TAG);
            return;
        }

        //Nếu TAG rỗng thì show Frg Splash
        showFrg(FrgSplash.TAG);
    }

    public void showFrg(String tag){
        try {
            Class<?> clazz = Class.forName(tag);
            Constructor<?> cons = clazz.getConstructor();
            BaseFragment fragment = (BaseFragment) cons.newInstance();
            fragment.setOnActionCallBack(this);
            getSupportFragmentManager().
                    beginTransaction()
                    .replace(R.id.ll_main,fragment,tag)
                    .commit();
            TAG = tag;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    public void showFrgLogin() {
        showFrg(LoginFrg.TAG);
    }

    @Override
    public void showFrgMenu() {
        showFrg(FrgPart2.TAG);
    }
}
