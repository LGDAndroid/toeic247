package com.techja.toeic247.callback;

public interface OnSignUpCallBackToView extends OnCallBackToView{
    void showNotify(String message);
}
