package com.techja.toeic247.callback;

public interface OnSignInCallBackToView extends OnCallBackToView{
    void showNotify(String message);

    void showFrgMenu();
}
