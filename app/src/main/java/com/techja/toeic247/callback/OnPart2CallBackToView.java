package com.techja.toeic247.callback;

public interface OnPart2CallBackToView extends OnCallBackToView{
    void setTvNumber(int current, int total);
}
