package com.techja.toeic247.model;

public class Audio {
    private String url;
    private String question;
    private String answer1, answer2, answer3;
    private int keyAnswer;

    public Audio(){

    }

    public Audio(String url, String question, String answer1, String answer2, String answer3, int keyAnswer) {
        this.url = url;
        this.question = question;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.keyAnswer = keyAnswer;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public int getKeyAnswer() {
        return keyAnswer;
    }

    public void setKeyAnswer(int keyAnswer) {
        this.keyAnswer = keyAnswer;
    }
}
