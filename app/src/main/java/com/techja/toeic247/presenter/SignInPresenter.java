package com.techja.toeic247.presenter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.techja.toeic247.callback.OnSignInCallBackToView;

public class SignInPresenter extends BasePresenter<OnSignInCallBackToView> {
    public SignInPresenter(OnSignInCallBackToView event) {
        super(event);
    }

    public void dangNhap(String email, String pass, Context mContext) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            mListener.showFrgMenu();
                            mListener.showNotify("Đăng nhập thành công!");
                        } else {
                            mListener.showNotify("Xác thực thất bại!");
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mListener.showNotify(e.getMessage());
            }
        })
        ;
    }
}
