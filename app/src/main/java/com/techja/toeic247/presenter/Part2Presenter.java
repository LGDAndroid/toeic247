package com.techja.toeic247.presenter;

import android.util.Log;

import com.techja.toeic247.callback.OnPart2CallBackToView;
import com.techja.toeic247.utils.MDatabase;
import com.techja.toeic247.utils.MediaManager;

public class Part2Presenter extends BasePresenter<OnPart2CallBackToView> {
    public Part2Presenter(OnPart2CallBackToView event) {
        super(event);
    }

    public void loadAudio() {
        Log.e("TAG",MDatabase.getInstance().getListAudio().size()+"");
        MediaManager.getInstance().loadListAudio();
        int current = MediaManager.getInstance().getCurrentIndex() + 1;
        int total = MediaManager.getInstance().getListSong().size();
        mListener.setTvNumber(current, total);
    }
}
