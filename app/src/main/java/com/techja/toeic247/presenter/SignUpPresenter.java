package com.techja.toeic247.presenter;

import android.app.Activity;
import android.content.Context;
import android.util.Patterns;
import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.techja.toeic247.callback.OnSignUpCallBackToView;

import java.util.HashMap;

public class SignUpPresenter extends BasePresenter<OnSignUpCallBackToView>{
    public SignUpPresenter(OnSignUpCallBackToView event) {
        super(event);
    }

    public void dangKy(String email, String pass, String checkPass, Context mContext) {
        if(!pass.equals(checkPass)){
            mListener.showNotify("Mật khẩu không trùng khớp!");
            return;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            mListener.showNotify("Email không hợp lệ!");
            return;
        }

        if(pass.length()<6){
            mListener.showNotify("Mật khẩu phải lớn hơn 6 ký tự");
            return;
        }

        register(email, pass, mContext);
    }

    private void register(String email, String pass, Context mContext) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();

                            String email = user.getEmail();
                            String uid = user.getUid();

                            HashMap<Object, String> hashMap = new HashMap<>();

                            hashMap.put("email", email);
                            hashMap.put("uid", uid);
                            hashMap.put("name", ""); //Bo sung sau

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference reference = database.getReference("Users");
                            reference.child(uid).setValue(hashMap);

                            mListener.showNotify("Đăng ký thành công!");
                        } else{
                            mListener.showNotify("Đăng ký không thành công!");
                        }
                    }
                }) .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mListener.showNotify(e.getMessage());
            }
        });
    }
}
