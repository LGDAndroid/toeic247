package com.techja.toeic247.presenter;

import com.techja.toeic247.callback.OnLoginCallBackToView;

public class LoginPresenter extends BasePresenter<OnLoginCallBackToView> {
    public LoginPresenter(OnLoginCallBackToView event) {
        super(event);
    }
}
