package com.techja.toeic247.presenter;

import com.techja.toeic247.callback.OnHomeCallBackToView;
import com.techja.toeic247.view.base.BaseActivity;

public class HomePresenter extends BasePresenter<OnHomeCallBackToView> {
    public HomePresenter(OnHomeCallBackToView event) {
        super(event);
    }
}
