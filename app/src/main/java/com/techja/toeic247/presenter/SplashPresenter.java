package com.techja.toeic247.presenter;

import com.techja.toeic247.callback.OnSplashCallBackToView;

public class SplashPresenter extends BasePresenter<OnSplashCallBackToView>{
    public SplashPresenter(OnSplashCallBackToView event) {
        super(event);
    }

}
