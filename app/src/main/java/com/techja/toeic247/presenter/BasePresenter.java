package com.techja.toeic247.presenter;

import com.techja.toeic247.callback.OnCallBackToView;

public abstract class BasePresenter<T extends OnCallBackToView>{
    protected T mListener;

    public BasePresenter(T event) {
        this.mListener = event;
    }
}
